import axios from "axios";
import { Component } from "react";
import { Button } from "reactstrap";
import Test3 from "./Test3";

const yazi = "pazartesi";

export default class Test4 extends Component {
  constructor() {
    super();
    this.state = {
      goster: false,
    };
  }

  setGoster = () => {
    this.setState({
      goster: !this.state.goster,
    });
  };

  render() {
    return (
      <div style={{ backgroundColor: "yellow" }}>
        <Button onClick={this.setGoster}>Tıkla</Button>
        <br></br>
        {this.state.goster === true ? "Hello World" : "asdasd"}
        <br></br>
        <Test3 propsGoster={this.state.goster}></Test3>
        <br></br>
        <Button onClick={() => axios.get(`http://localhost:8080/api2/` + yazi)}>
          Mail
        </Button>
      </div>
    );
  }
}
