import React, { useState,useEffect } from "react";
import { Button } from "reactstrap";

export default function Test3(props) {
  const [goster, setGoster] = useState(false);

  useEffect(() => {
      console.log("degişti")
  }, [goster]);

  return (
    <>
      <div style={{backgroundColor:"blue"}}>
        <Button onClick={() => setGoster(!goster)}>Tıkla</Button>
        <br></br>
        {props.propsGoster === true ? "Hello World" : "asdasd"}
      </div>
    </>
  );
}
