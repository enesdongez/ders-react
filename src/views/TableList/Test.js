import React, { useState } from "react";
import { Button } from "reactstrap";

export default function Test() {
  const [show, setShow] = useState(false);

  return (
    <>
      <div>
        <Button onClick={() => setShow(null)}>Tikla</Button>
        <br></br>
        {show ? "Hello World" : ""}
      </div>
    </>
  );
}
