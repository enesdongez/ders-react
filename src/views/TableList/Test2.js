import React, { Component } from "react";
import axios from "axios";
import { Button } from "reactstrap";

export default class Test2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showText: null,
      state2:null,
      abc:"abc"
    };
  }

  sendMail = () => {
    axios
      .post(`http://localhost:8080/sendMail/Enes/Konu/Mesaj/a@a.com`)
      .then((res) => {});
  };

  show = () => {
    if (this.state.showText === true) {
      this.setState({
        showText: null,
      });
    } else {
      this.setState({
        showText: true,
      });
    }
  };

  render() {
    return (
      <>
        <div>
          <Button onClick={this.sendMail} style={{marginRight:"20px"}}>Mail</Button>
          <Button onClick={this.show}>text show</Button>
          <br></br>
          {this.state.showText ? "yazi gozukuyor" : ""}
        </div>
      </>
    );
  }

}
